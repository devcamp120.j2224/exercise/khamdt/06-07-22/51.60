package com.devcamp.j02_javabasic.s30;
public class j51 {
    public static void main(String[] args) { 
        j51 myj51 = new j51();
        myj51.duLieuNguyenThuy();
        myj51.duLieuobj();
    }
    public static void duLieuNguyenThuy(){
        Integer myInt = 5; 
        Double myDouble = 5.99; 
        Character myChar = 'A'; 
        System.out.println(myInt);
        System.out.println(myDouble);
        System.out.println(myChar);
    } 
    public void duLieuobj(){
        float fat = 10.0F; // floating point number
        char ch = 'C'; // character
        boolean bool = true; // boolean
        
        double dbl = 20.0D; // double
        byte bte = 30; // byte
        short sh = 40;
        int it = 50;
        long lng = 60;

        Byte byteobj = bte;
        Short shortobj = sh;
        Integer integerobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;

        byte bytevalue = byteobj;
        short shortvalue = shortobj;
        int intvalue = integerobj;
        long longvalue = longobj;
        float floatvalue = floatobj;
        double doublevalue = doubleobj;
        char charvalue = charobj;
        boolean boolvalue = boolobj;
        System.out.println("in gia tri cua value   ");
        System.out.println("in gia tri cua value   " + bytevalue);
        System.out.println("in gia tri cua value   " + shortvalue);
        System.out.println("in gia tri cua value   " + intvalue);
        System.out.println("in gia tri cua value   " + longvalue);
        System.out.println("in gia tri cua value   " + floatvalue);
        System.out.println("in gia tri cua value   " + doublevalue);
        System.out.println("in gia tri cua value   " + charvalue);
        System.out.println("in gia tri cua value   " + boolvalue);
    }
}
