package com.devcamp.j02_javabasic.s30.j5160;

import java.util.ArrayList;

public class j5160 {
    public static void main(String[] args) {
        // ArrayList<Integer> myNumbers = new ArrayList<Integer>();
        // myNumbers.add(10);
        // myNumbers.add(15);
        // myNumbers.add(20);
        // myNumbers.add(25);
        // for (int i : myNumbers) {
        // System.out.println(i);
        // }
        j5160 myj5160 = new j5160();
        myj5160.longArrayList();
        myj5160.DoubleArrayList();
     
    }
    public void longArrayList(){

        ArrayList<Integer> myNumbers = new ArrayList<Integer>();
       
        long[] b = new long[100];
        for (int i = 0; i < 100; i++) {
            b[i] = i;
            myNumbers.add(i);
            // System.out.println(i);
        }
        for (int j : myNumbers) {
            System.out.print(" "+j);
            }
    }
    public void DoubleArrayList(){
        ArrayList<Integer> myNumbers = new ArrayList<Integer>();
       
        double[] b = new double[100];
        for (int i = 0; i < 100; i++) {
            b[i] = i;
            myNumbers.add(i);
            // System.out.println(i);
        }
        for (int j : myNumbers) {
            System.out.print(" "+j);
            }
    }
}