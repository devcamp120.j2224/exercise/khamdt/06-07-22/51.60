public class wrapperclass  {

    public static void main(String[] args) throws Exception {
        // wrapperclass.autoBoxing();
        // wrapperclass.unBoxing();
    }

    public static void autoBoxing() {
        
        float fat = 5.00f; // floating point number
        char ch = 'a'; // character
        boolean bool = true; // boolean
        
        double dbl = 60.0D; // double
        byte bte = 10; // byte
        short sh = 20;
        int it = 30;
        long lng = 40;

        Byte byteobj = bte;
        Short shortobj = sh;
        Integer integerobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;

        System.out.println("in gia tri cua object");
        System.out.println("in gia tri cua object " + byteobj);
        System.out.println("in gia tri cua object " + shortobj);
        System.out.println("in gia tri cua object " + integerobj);
        System.out.println("in gia tri cua object " + longobj);
        System.out.println("in gia tri cua object " + floatobj);
        System.out.println("in gia tri cua object " + doubleobj);
        System.out.println("in gia tri cua object " + charobj);
        System.out.println("in gia tri cua object " + boolobj);
        
    }

    ///////////////////////////////
    //////////////////////////////
    public static void unBoxing() {
        float fat = 10.0F; // floating point number
        char ch = 'C'; // character
        boolean bool = true; // boolean
        
        double dbl = 20.0D; // double
        byte bte = 30; // byte
        short sh = 40;
        int it = 50;
        long lng = 60;

        Byte byteobj = bte;
        Short shortobj = sh;
        Integer integerobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;

        byte bytevalue = byteobj;
        short shortvalue = shortobj;
        int intvalue = integerobj;
        long longvalue = longobj;
        float floatvalue = floatobj;
        double doublevalue = doubleobj;
        char charvalue = charobj;
        boolean boolvalue = boolobj;
        System.out.println("in gia tri cua value   ");
        System.out.println("in gia tri cua value   " + bytevalue);
        System.out.println("in gia tri cua value   " + shortvalue);
        System.out.println("in gia tri cua value   " + intvalue);
        System.out.println("in gia tri cua value   " + longvalue);
        System.out.println("in gia tri cua value   " + floatvalue);
        System.out.println("in gia tri cua value   " + doublevalue);
        System.out.println("in gia tri cua value   " + charvalue);
        System.out.println("in gia tri cua value   " + boolvalue);

    }
}
